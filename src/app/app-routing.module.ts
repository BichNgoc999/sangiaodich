import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
//import { LoginComponent } from './login/login.component';
import { MarketComponent } from './market/market.component';
//import { RegisterComponent } from './register/register.component';
//import { UserInforComponent } from './user-infor/user-infor.component';


const routes: Routes = [
  { path: '', component: MarketComponent },
  //{ path: 'login', component: LoginComponent },
  //{ path: 'register', component: RegisterComponent },
  //{ path: 'profile', component: UserInforComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
